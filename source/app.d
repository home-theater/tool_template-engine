import std.algorithm : canFind;
import std.exception : enforce;
import std.file;
import std.stdio;
import std.string : f = format, lineSplitter, split, replaceInPlace;

struct Block
{
  string condition;
  Block[] children;
  string contents;
  bool leaf;

  /// non-leaf ctor
  this(string condition, Block[] children)
  {
    this.condition = condition;
    this.children = children;
    this.leaf = false;
  }

  /// leaf ctor
  this(string contents)
  {
    this.contents = contents;
    this.leaf = true;
  }

  static Block fromString(string s)
  {
    Block[] stack = [Block("true", [])];
    foreach (line; s.lineSplitter)
    {
      if (line.length > 6 && line[0 .. 3] == "{% " && line[$ - 3 .. $] == " %}")
      {
        // Special line
        if (line.length > 10 && line[3 .. 7] == "end ")
        {
          // {% end ... %}
          string begin_cond = stack[$ - 1].condition;
          string end_cond = line[7 .. $ - 3];
          enforce(
            end_cond == stack[$ - 1].condition,
            f!"End statement '%s' does not match begin statement '%s'."(begin_cond, end_cond)
          );
          stack[$ - 2].children ~= stack[$ - 1];
          stack = stack[0 .. $ - 1];
        }
        else
        {
          // {% ... %}
          Block b = Block(line[3 .. $ - 3], []);
          stack ~= b;
        }
      }
      else
      {
        // Regular line
        Block* currLeaf;
        if (stack[$ - 1].children.length > 0 && stack[$ - 1].children[$ - 1].leaf)
        {
          // Last node is a leaf
          currLeaf = &stack[$ - 1].children[$ - 1];
        }
        else
        {
          // Last node is not a leaf, create one
          stack[$ - 1].children ~= Block("");
          currLeaf = &stack[$ - 1].children[$ - 1];
        }
        (*currLeaf).contents ~= line ~ "\n";
      }
    }
    return stack[0];
  }

  string substitute(string[] variables)
  {
    if (leaf)
      return substituteInline(variables);

    string substituteChildren()
    {
      string result;
      foreach (child; children)
        result ~= child.substitute(variables);
      return result;
    }

    if (condition == "true")
      return substituteChildren;
    else if (condition == "false")
      return "";
    if (condition.length > 3 && condition[0 .. 3] == "if ")
    {
      // cond is an if statement
      string condBody = condition[3 .. $];
      enforce(!condBody.canFind(" "), f!"if condition '%s' has spaces"(condBody));
      enforce(!condBody.canFind("="), f!"if condition '%s' has ="(condBody));
      if (variables.canFind(condition[3 .. $]))
        return substituteChildren;
      else
        return "";
    }
    else
      enforce(false, f!"Invalid condition '%s'"(condition));
    assert(false);
  }

  /// Replaces {~ ... ~} sequences
  string substituteInline(string[] variables)
  in (leaf)
  {
    string[string] substitutionVariables; // All A=B variables

    foreach (var; variables)
    {
      if (var.canFind("="))
      {
        string[] split = var.split("=");
        enforce(split.length == 2, f!"Substitution variable '%s' has multiple = signs"(var));
        enforce(!var.canFind(" "), f!"Substitution variable '%s' has spaces"(var));
        enforce(split[0].length > 0, f!"Substitution variable '%s' has an empty name"(var));
        substitutionVariables[split[0]] = split[1];
      }
    }

    string result;
    foreach (line; contents.lineSplitter!(KeepTerminator.yes))
    {
      for (int i = 0; i < line.length; i++)
      {
        if (line.length - i >= 3 + 1 + 3 && line[i .. i + 3] == "{~ "
          && line[i + 4 .. $].canFind(" ~}"))
        {
          string substituteBody;
          int endIndex;
          for (int j = i; j < line.length; j++)
          {
            assert(j + 2 < line.length);
            if (line[j .. j + 3] == " ~}")
            {
              substituteBody = line[i + 3 .. j];
              endIndex = j + 2;
              break;
            }
          }
          enforce(substituteBody in substitutionVariables, f!"Unknown substitution variable '%s'"(
              substituteBody));
          line.replaceInPlace(i, endIndex + 1, substitutionVariables[substituteBody]);
          i = 0;
        }
      }
      result ~= line;
    }
    return result;
  }
}

unittest
{
  string s;
  s ~= "Title\n";
  s ~= "{% if MOBILE %}\n";
  s ~= "touch stuff\n";
  s ~= "{% if ANDROID %}\n";
  s ~= "apk stuff\n";
  s ~= "{% end if ANDROID %}\n";
  s ~= "{% end if MOBILE %}\n";

  Block root = Block.fromString(s);
  Block expected = Block("true", [
      Block("Title\n"),
      Block("if MOBILE", [
          Block("touch stuff\n"),
          Block("if ANDROID", [
            Block("apk stuff\n")
          ])
        ])
    ]);
  assert(root == expected);
}

unittest
{
  Block b = Block("true", [
      Block("Title\n"),
      Block("if MOBILE", [
          Block("touch stuff\n"),
          Block("if ANDROID", [
            Block("apk stuff\n")
          ])
        ])
    ]);
  assert(b.substitute([]) == "Title\n");
  assert(b.substitute(["ANDROID"]) == "Title\n");
  assert(b.substitute(["MOBILE"]) == "Title\ntouch stuff\n");
  assert(b.substitute(["ANDROID", "MOBILE"]) == "Title\ntouch stuff\napk stuff\n");
  assert(b.substitute(["MOBILE", "ANDROID"]) == "Title\ntouch stuff\napk stuff\n");
}

unittest
{
  Block b = Block(
    "Platform: {~ PLATFORM ~}, Architecture: {~ ARCH ~}, Tuple: {~ ARCH ~}{~ PLATFORM ~}");
  string result = b.substitute(["PLATFORM=Android", "ARCH=arm"]);
  string expected = "Platform: Android, Architecture: arm, Tuple: armAndroid";
  assert(result == expected);
}

int main(string[] args)
{
  void usage()
  {
    writeln(
      "Usage: template-engine [input file] [output file] [optional comma-separated variable list]"
    );
  }

  if (!(args.length == 3 || args.length == 4))
  {
    usage;
    return 1;
  }

  string varsString = args.length == 4 ? args[3] : "";
  string[] variables;
  try
  {
    variables = varsString.split(",");
  }
  catch (Exception e)
  {
    enforce(false, f!"Error reading variables %s: %s"(varsString, e));
  }

  string inputFilePath = args[1];
  string inputFileContents;
  try
  {
    enforce(exists(inputFilePath), "File doesn't exist");
    inputFileContents = std.file.readText(inputFilePath);
  }
  catch (Exception e)
  {
    enforce(false, f!"Error reading file %s: %s"(inputFilePath, e));
  }

  Block root = Block.fromString(inputFileContents);
  string outputFileContents = root.substitute(variables);

  string outputFilePath = args[2];
  try
  {
    std.file.write(outputFilePath, outputFileContents);
  }
  catch (Exception e)
  {
    enforce(false, f!"Error writing file %s: %s"(outputFilePath, e));
  }
  return 0;
}
